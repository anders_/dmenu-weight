{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -fdefer-typed-holes #-}
module Main (main) where

import Control.Exception
import Control.Monad
import Data.Binary
import Data.Char
import Data.List
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Ord
import Data.Semigroup
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO
import System.Posix.Files
import System.Posix.Types
import System.Process

main :: IO ()
main = do
    args <- getArgs
    case args of
      []        -> return ()
      ["clear"] -> do clear; exitSuccess
      _         -> do hPutStrLn stderr "usage: $0 [clear]"; exitFailure
    exes'   <- executables
    weights <- readWeights
    let exes = sortBy (compareWeights weights) exes'
    cmd <- readProcess "dmenu" ["-i", "-p", "run:"] (unlines exes)
           `catch` \(_ :: IOError) -> exitFailure
    let exe = takeWhile (not . isSpace) cmd
    writeWeights (incr exe weights)
    system cmd >>= exitWith


clear :: IO ()
clear = do
    filename <- getDataFile
    exists   <- doesFileExist filename
    when exists $ removeFile filename

getDataDir :: IO FilePath
getDataDir = do
    prog <- getProgName
    getXdgDirectory XdgData prog

getDataFile :: IO FilePath
getDataFile = (</> "weights") <$> getDataDir


readWeights :: IO (Map String Int)
readWeights = do
    filename   <- getDataFile
    fileExists <- doesFileExist filename
    if fileExists then
      decodeFile filename
    else
      return Map.empty

writeWeights :: Map String Int -> IO ()
writeWeights weights = do
    dirname <- getDataDir
    createDirectoryIfMissing True dirname
    setFileMode dirname (CMode 0o700)
    encodeFile (dirname </> "weights") weights


incr :: String -> Map String Int -> Map String Int
incr = Map.alter (Just . maybe 1 succ)


compareWeights :: Map String Int -> String -> String -> Ordering
compareWeights weights = flip (comparing weight) <> compare
  where weight x = Map.findWithDefault 0 x weights

executables :: IO [String]
executables = do
    paths <- splitChar ':' <$> getEnv "PATH"
    concat <$> mapM getFiles paths

splitChar :: Char -> String -> [String]
splitChar c xs =
    case break (== c) xs of
      (x, "")   -> [x]
      (x, _:ys) -> x : splitChar c ys

getFiles :: String -> IO [String]
getFiles dir = do
    ex <- doesDirectoryExist dir
    if ex then do
      files <- map (dir </>) <$> listDirectory dir
      exes  <- filterM (fmap executable . getPermissions) files
      return (map takeFileName exes)
    else
      return []
